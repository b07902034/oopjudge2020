public class GreenCrud {
	public static int calPopulation (int initialSize, int days) {
		//because green grud grows every five days, so I divide days by 5                 
		days /=  5;  
		//since there is at most 100 days, I declare an array with size 21                                      
		int[] result;                                      
		result = new int[21];
		//set the first number as the initial size
		result[0] = initialSize;
		//for loop which implements Fibonacci sequence
		for (int i = 1; i <= days; i++) {
			if (i == 1)
				result[i] = result[0];
			else 
				result[i] = result[i-1] + result[i-2];
		}
		//return the result
		if (days > 0)
			return result[days];
		else
			return result[0];
	}
}