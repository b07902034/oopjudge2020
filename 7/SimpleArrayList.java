public class SimpleArrayList {
	// information of SimpleArrayList
	private int index;
	private Integer array[];
	// default constructor
	public SimpleArrayList() {
		this.index = 0;
		this.array = new Integer[0];
	}
	// set the constructor
	public SimpleArrayList(int index) {
		this.index = index;
		this.array = new Integer[index];
		for(int i = 0; i < this.index; i++)
			this.array[i] = 0;
	}
	// add an integer at the end of array
	public void add(Integer i) {
		this.index += 1;
		Integer tmp[] = new Integer[this.index];
		for(int k = 0; k < this.index; k++) {
			if(k == this.index-1)
				tmp[k] = i;
			else
				tmp[k] = this.array[k]; 
		}
		this.array = tmp;
	}
	// get the specific position in the array
	public Integer get(int index) {
		if(this.index <= index)
			return null;
		else
			return this.array[index];
	}
	// set the specific position with the given element
	public Integer set(int index, Integer element) {
		if(this.index <= index)
			return null;
		else {
			Integer tmp = this.array[index];
			this.array[index] = element;
			return tmp;
		}
	}
	// remove the specific position in the array
	public boolean remove(int index) {
		if((this.index <= index) || (this.array[index] == null)) 
			return false;
		else {
			this.index -= 1;
			Integer tmp[] = new Integer[this.index];
			int j = 0;
			for(int i = 0; i < this.index; i++) {
				if(j == index)
					j += 1;
				tmp[i] = this.array[j];
				j += 1;
			}
			this.array = tmp;
			return true;
		}
	}
	// clear all elements in the array
	public void clear() {
		this.index = 0;
		this.array = new Integer[0];
	}
	// return the size of the array
	public int size() {
		return this.index;
	}
	// retains only the elements in this array that are contained in the specific SimpleArrayList
	public boolean retainAll(SimpleArrayList l) {
		// the number of elements that are removed
		int cnt = 0;
		// check whether the elements in this array exist in the specific SimpleArrayList
		for(int i = 0; i < this.index; i++) {
			int flag = 0;
			for(int j = 0; j < l.size(); j++) {
				if(this.array[i] == l.array[j]) {
					flag = 1;
					break;
				}
			}
			if(flag == 0) {
				this.remove(i);
				i -= 1;
				cnt += 1;
			}
		}
		if(cnt > 0)
			return true;
		else
			return false;
	}
}