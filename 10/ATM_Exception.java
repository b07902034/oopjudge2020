public class ATM_Exception extends Exception {
	private ExceptionTYPE exceptionCondition;
	// includes two kinds of exception
	public enum ExceptionTYPE {
		BALANCE_NOT_ENOUGH,
		AMOUNT_INVALID
	}
	// set exceptionCondition by constructor
	public ATM_Exception(ExceptionTYPE ex_type) {
		this.exceptionCondition = ex_type;
	}
	// return information of exception
	public String getMessage() {
		return this.exceptionCondition.toString();
	}
}