public class Simple_ATM_Service implements ATM_Service {
	// check whether balance in user's account is sufficient
	public boolean checkBalance(Account account, int money) throws ATM_Exception {
		// if not, throws an exception
		if (account.getBalance() < money)
			throw new ATM_Exception(ATM_Exception.ExceptionTYPE.BALANCE_NOT_ENOUGH);
		return true;
	}
	// check if amount of money can be divided by 1000
	public boolean isValidAmount(int money) throws ATM_Exception {
		// if not, throws an exception
		if (money % 1000 != 0)
			throw new ATM_Exception(ATM_Exception.ExceptionTYPE.AMOUNT_INVALID);
		return true;
	}
	// first calls checkBalance, then calls isValidAmount
	public void withdraw(Account account, int money) {
		try {
			if (checkBalance(account, money))
				if (isValidAmount(money))
					account.setBalance(account.getBalance() - money);
		} catch (ATM_Exception ex) {
			System.out.println(ex.getMessage());
		} finally {
			// always print updated balance
			System.out.println("updated balance : " + account.getBalance());
		}
	}
}