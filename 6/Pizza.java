public class Pizza {
	//information of Pizza	
	private String Size;
	private int NumberOfCheese;
	private int NumberOfPepperoni;
	private int NumberOfHam;
	//if there are no parameters, set them by default
	public Pizza() {
		this("small", 1, 1, 1);
	}
	//if there parameters, then set the information
	public Pizza(String Size, int NumberOfCheese, int NumberOfPepperoni, int NumberOfHam) {
		this.Size = Size;
		this.NumberOfCheese = NumberOfCheese;
		this.NumberOfPepperoni = NumberOfPepperoni;
		this.NumberOfHam = NumberOfHam;
	}

	//set the information of Pizza by function
	public void setSize(String Size) {
		this.Size = Size;
	}
	public void setNumberOfCheese(int NumberOfCheese) {
		this.NumberOfCheese = NumberOfCheese;
	}
	public void setNumberOfPepperoni(int NumberOfPepperoni) {
		this.NumberOfPepperoni = NumberOfPepperoni;
	}
	public void setNumberOfHam(int NumberOfHam) {
		this.NumberOfHam = NumberOfHam;
	}

	//return the information of Pizza by function
	public String getSize() {
		return Size;
	}
	public int getNumberOfCheese() {
		return NumberOfCheese;
	}
	public int getNumberOfPepperoni() {
		return NumberOfPepperoni;
	}
	public int getNumberOfHam() {
		return NumberOfHam;
	}

	//calculate the cost depending on the size
	public double calcCost() {
		if (Size == "small")
			return (10 + 2*(NumberOfCheese+NumberOfPepperoni+NumberOfHam));
		else if (Size == "medium")
			return (12 + 2*(NumberOfCheese+NumberOfPepperoni+NumberOfHam));
		else
			return (14 + 2*(NumberOfCheese+NumberOfPepperoni+NumberOfHam));
	}

	//return the information of Pizza
	public String toString() { 
		return "size = "+Size+", numOfCheese = "+String.valueOf(NumberOfCheese)+", numOfPepperoni = "+String.valueOf(NumberOfPepperoni)+", numOfHam = "+String.valueOf(NumberOfHam);
	}

	//check whether two Pizza's information are the same
	public boolean equals(Pizza OtherPizza) {
		if (OtherPizza.getSize() == Size && OtherPizza.getNumberOfCheese() == NumberOfCheese && OtherPizza.getNumberOfPepperoni() == NumberOfPepperoni && OtherPizza.getNumberOfHam() == NumberOfHam)
			return true;
		else
			return false;
	}
}