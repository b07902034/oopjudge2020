public class PizzaOrder {
	// information of PizzaOrder
	private int numberPizzas;
	private Pizza pizza1;
	private Pizza pizza2;
	private Pizza pizza3;
	// return true if 1 <= numberPizzas <= 3, else return false
	public boolean setNumberPizzas(int numberPizzas) {
		if (numberPizzas >= 1 && numberPizzas <= 3) {
			this.numberPizzas = numberPizzas;
			return true;
		}
		else 
			return false;
	}
	// set the information of pizza
	public void setPizza1(Pizza pizza1) {
		this.pizza1 = pizza1;
	}
	public void setPizza2(Pizza pizza2) {
		this.pizza2 = pizza2;
	}
	public void setPizza3(Pizza pizza3) {
		this.pizza3 = pizza3;
	}
	// calculate total cost according to the number of numberPizzas
	public double calcTotal() {
		if (this.numberPizzas == 1) 
			return this.pizza1.calcCost();
		else if (numberPizzas == 2)
			return this.pizza1.calcCost() + this.pizza2.calcCost();
		else
			return this.pizza1.calcCost() + this.pizza2.calcCost() + this.pizza3.calcCost();
	}
}