public class SentenceProcessor {
	public static String removeDuplicatedWords (String sentence) {
		String[] words = sentence.split(" ");
		for (int i = 0; i < words.length; i++) {
			if (words[i] != null) 
				for (int j = i+1; j < words.length; j++) {
					if (words[i].equals(words[j]))
						words[j] = null;
				}
		}
		String result = "";
		for (int i = 0; i < words.length; i++) {
			if (words[i] != null) {
				if (result.length() == 0) 
					result += words[i];
				else
					result += (" " + words[i]);
			}
		}
		return result;
	}
	public static String replaceWord (String target, String replacement, String sentence) {
		String[] words = sentence.split(" ");
		for (int i = 0; i < words.length; i++) {
			if (words[i].equals(target))
				words[i] = replacement;
		}
		String result = "";
		for (int i = 0; i < words.length; i++) {
			if (result.length() == 0) 
				result += words[i];
			else
				result += (" " + words[i]);
		}
		return result;
	}
}