public class Circle extends Shape {
	// initialize with parameter
	public Circle(double length) {
		super(length);
	}
	// set the diameter of circle
	public void setLength(double length) {
		this.length = length;
	}
	// calculate the area of circle
	public double getArea() {
		double radius = this.length/2;
		double area = Math.PI * Math.pow(radius, 2);
		return Math.round(area * 100.0) / 100.0;
	}
	// calculate the perimeter of circle
	public double getPerimeter() {
		double radius = this.length/2;
		double perimeter = 2 * Math.PI * radius;
		return Math.round(perimeter * 100.0) / 100.0;
	}
}