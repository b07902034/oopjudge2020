public class Square extends Shape {
	// initialize with parameter
	public Square(double length) {
		super(length);
	}
	// set the side length of square
	public void setLength(double length) {
		this.length = length;
	}
	// calculate the area of square
	public double getArea() {
		double area = Math.pow(this.length, 2);
		return Math.round(area * 100.0) / 100.0;
	}
	// calculate the perimeter of square
	public double getPerimeter() {
		double perimeter = this.length * 4;
		return Math.round(perimeter * 100.0) / 100.0;
	}
}