public class ShapeFactory {
	// store all kinds of shape
	public enum Type {
		Triangle, 
		Circle, 
		Square
	}
	// create instance of all shapes refer to parameter
	public Shape createShape(ShapeFactory.Type shapeType, double length) {
		if(shapeType.toString() == "Triangle") 
			return new Triangle(length);
		if(shapeType.toString() == "Circle")
			return new Circle(length);
		if(shapeType.toString() == "Square") 
			return new Square(length);
		return null;
	}
}