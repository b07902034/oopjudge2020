public class Triangle extends Shape {
	// initialize with parameter
	public Triangle(double length) {
		super(length);
	}
	// set the side length of triangle
	public void setLength(double length) {
		this.length = length;
	}
	// calculate the area of triangle
	public double getArea() {
		double area = Math.sqrt(3)/4 * Math.pow(this.length, 2);
		return Math.round(area * 100.0) / 100.0;
	}
	// calculate the perimeter of triangle
	public double getPerimeter() {
		double perimeter = this.length * 3;
		return Math.round(perimeter * 100.0) / 100.0;
	}
}