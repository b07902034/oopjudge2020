import java.text.DecimalFormat;
public class SimpleCalculator {
	// information of SimpleCalculator
	private double result = 0.00;
	private int count = 0;
	private String operator;
	private double value;
	private boolean end = false;
	// calculate the result if no error occurs
	public void calResult(String cmd) throws UnknownCmdException {
		String[] arr = cmd.split(" ");
		// error: wrong format
		if(arr.length != 2)
			throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
		// error: unknown operator and unknown value
		if(!(arr[0].equals("+") || arr[0].equals("-") || arr[0].equals("*") || arr[0].equals("/"))) {
			for(int i = 0; i < arr[1].length(); i++) {
				if(arr[1].charAt(i) == 47 || arr[1].charAt(i) < 46 || arr[1].charAt(i) > 57) 
					throw new UnknownCmdException(arr[0] + " is an unknown operator and " + arr[1] + " is an unknown value");
			}
			//error: unknown operator
			throw new UnknownCmdException(arr[0] + " is an unknown operator");
		}
		// error: unknown value
		for(int i = 0; i < arr[1].length(); i++) {
			if(arr[1].charAt(i) == 47 || arr[1].charAt(i) < 46 || arr[1].charAt(i) > 57)
				throw new UnknownCmdException(arr[1] + " is an unknown value");
		}
		// error: divide by zero
		if(arr[0].equals("/")) {
			int cnt = 0;
			for(int i = 0; i < arr[1].length(); i++) {
				if(arr[1].charAt(i) == 46 || arr[1].charAt(i) == 48) 
					cnt += 1;
			}
			if(cnt == arr[1].length())
				throw new UnknownCmdException("Can not divide by 0");
		}
		// calculate the result depends on the operator
		this.operator = arr[0];
		this.value = Double.valueOf(arr[1]);
		if(this.operator.equals("+"))
			this.result += this.value;
		if(operator.equals("-"))
			this.result -= this.value;
		if(operator.equals("*"))
			this.result *= this.value;
		if(operator.equals("/"))
			this.result /= this.value;
		this.count += 1;
	}
	// return the result rounded off to the 2nd decimal place
	public String getMsg() {
		DecimalFormat df = new DecimalFormat("0.00");
		String str1 = String.valueOf(df.format(this.result));
		String str2 = String.valueOf(df.format(this.value));
		if(this.end)
			return("Final result = " + str1);
		if(this.count == 0) 
			return("Calculator is on. Result = " + str1);
		else if(this.count == 1)
			return("Result " + this.operator + " " + str2 + " = " + str1 + ". New result = " + str1); 
		else
			return("Result " + this.operator + " " + str2 + " = " + str1 + ". Updated result = " + str1);

	}
	// check whether cmd is "r" or "R"
	public boolean endCalc(String cmd) {
		if(cmd.equals("r") || cmd.equals("R")) {
			this.end = true;
			return true;
		}
		else
			return false;
	}
}