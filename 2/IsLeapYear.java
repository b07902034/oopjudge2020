public class IsLeapYear {
	public static boolean determine (int year) {  //year is the parameteer that we want to determine
		if (year % 4 == 0 && year % 100 != 0)     //if year is the multiple of 4 and not the multiple of 100, then it is leap year
			return true;
		else if (year % 400 == 0)				  //if year is the multiple of 400, then it is also leap year
			return true;
		else                                      //other situations are not leap year
			return false;
	}
}